(function() {
  'use strict';
var app = angular.module('application', [
    'ui.router',
    'ngAnimate',
    'xeditable',
    'ui.grid',
//    'ui.grid.auto.Resize',
    'ui.grid.resizeColumns',
    'ui.grid.moveColumns',
    'ui.grid.pinning',
    'ui.grid.expandable',
    'ui.grid.selection',

    //foundation
    'foundation',
    'foundation.dynamicRouting',
    'foundation.dynamicRouting.animations',
    // introjs
    'angular-intro'

  ])
    .config(config)
    .run(run)
  ;

  config.$inject = ['$urlRouterProvider', '$locationProvider'];

  function config($urlProvider, $locationProvider) {
    $urlProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled:false,
      requireBase: false
    });

    $locationProvider.hashPrefix('!');
  }

  function run() {
    FastClick.attach(document.body);
  }


app.run(function(editableOptions) {
  editableOptions.theme = 'default'; // bootstrap3 theme. Can be also 'bs2', 'default'
});









//})();
